#include "Board.h"
#include <vector>
#include <iostream>
#include <cmath>
using namespace std;

vector<Node> AStar(int startX, int startY, int goalX, int goalY, Board board);

void addNodeToList(int x, int y, vector<Node*>& list,Board& board);

void generateSuccessors(int goalX, int goalY, vector<Node*>& openList, vector<Node*> closedList, Board& board,Node* parent);

void removeNodeFromList(int x, int y,vector<Node*>& list);

void removeAllFromList(vector<Node*> list);

bool inList(vector<Node*> list, Node* node);

Node* getNode(vector<Node*> list, int x, int y);

double getScore(int currentX, int currentY, int goalX,int goalY);

double manhattenDistance(int currentX, int currentY, int goalX, int goalY);

double euclideanDistance(int x, int y, int goalX, int goalY);



