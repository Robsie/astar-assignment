#pragma once
#ifndef NODELIST_H
#define NODELIST_H

#include <vector>
#include "Node.h"

using namespace std;

//The main point of this class is to encompass methods previously in the astar.cpp file, acting on a vector of Node pointers
//But also to ensure that when going out of scope all used memory will be destroyed to prevent memory leaks, on the downside because pointers are used its possible they will be used in others lists,
//so probably best to be careful with usage
class NodeList
{
private:
	//member variables
	vector<Node*> _list;
protected:
public:
	//methods
	NodeList();
	~NodeList();
	Node* getItem(int x, int y);
	bool inList(Node* node);
	void erase(int x, int y);
	void push_back(Node*);
	void addNew(int x, int y, int cost);
	void remove(int x, int y);
	int size();

	//operators
	Node* operator[](int);
};

#endif