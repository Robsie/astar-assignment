#include "Board.h"

//Private methods
//path finding
double Board::euclideanDistance(int currentX, int currentY,int goalX, int goalY)
{
	Coordinates current(currentX,currentY);
	Coordinates goal(goalX,goalY);
	return euclideanDistance(current,goal);
}
double Board::euclideanDistance(Coordinates current,Coordinates goal)
{
	return sqrt(pow(current.X-goal.X,2.0) + pow(current.Y-goal.Y,2.0));
}
double Board::manhattenDistance(int currentX, int currentY,int goalX, int goalY)
{
	Coordinates current(currentX,currentY);
	Coordinates goal(goalX,goalY);
	return manhattenDistance(current,goal);
}
double Board::manhattenDistance(Coordinates current,Coordinates goal)
{
	return (abs(current.X-goal.X) + abs(current.Y-goal.Y));
}

double Board::chebyshevDistance(int currentX, int currentY,int goalX, int goalY)
{
	Coordinates current(currentX,currentY);
	Coordinates goal(goalX,goalY);
	return chebyshevDistance(current,goal);
}
double Board::chebyshevDistance(Coordinates current,Coordinates goal)
{
	return max(abs(goal.X-current.X),abs(goal.Y-current.Y));
}

Board::Board(int r, int c) : TwoDimensionalArray(r,c)
{
}

Board::Board(int r, int c, int* d) : TwoDimensionalArray(r,c,d)
{
}

//PATH FINDING METHODS
vector<Coordinates> Board::findPath(int startX, int startY, int goalX, int goalY,int type,int heuristic)
{
	Coordinates start(startX, startY);
	Coordinates goal(goalX,goalY);
	return findPath(start,goal,type,heuristic);
}

vector<Coordinates> Board::findPath(Coordinates start, Coordinates goal, int type,int heuristic)
{
	NodeList openList;
	NodeList closedList;
	vector<Coordinates> bestPath;
	if(type == 0)
	{
		bestPath = astar(start,goal,heuristic);
	}
	/*else if(type == 1)
	{
		bestPath = astarBidirectional(start,goal,heuristic);
	}*/
	return bestPath;
}

void Board::generateSuccessors(Coordinates goal, NodeList& openList, NodeList& closedList, Node* parent,int heuristic)
{
	int x = (*parent).X();
	int y = (*parent).Y();
	for(int i = x - 1; i<=x+1;i++)
	{
		for(int j = y - 1; j<= y +1;j++)
		{
			if(i != x || j != y)
			{
				if(GetItem(i,j) != 0 && i >=0 && j >=0 )
				{
				//	cout<<"instance: "<<i<<" "<<j<<endl;
					//double h = manhattenDistance(i,j,goalX,goalY);
					double h = 0.0;
					if(heuristic == 0)
					{
						h = manhattenDistance(i,j,goal.X,goal.Y);

					}
					if(heuristic == 1)
					{
						h = euclideanDistance(i,j,goal.X,goal.Y);

					}
					else if(heuristic == 2)
					{
						h = chebyshevDistance(i,j,goal.X,goal.Y);
					}
					else
					{
						h = manhattenDistance(i,j,goal.X,goal.Y);
					}
					
					Node* newNode =  new Node(i,j,GetItem(i,j),h,parent);
					if(closedList.inList(newNode)) // if its already in the closed list
					{
						//update the existing one to the new parent and g value
						Node* current = closedList.getItem((*newNode).X(),(*newNode).Y());
						if( (*newNode).G() < (*current).G())
						{
							(*current).G((*newNode).G());
							(*current).Parent((*newNode).Parent());
							delete newNode;
						}
					}
					else if(openList.inList(newNode)) // if its already in the open list
					{
						//update the existing one to the new parent and p value
						Node* current = openList.getItem((*newNode).X(),(*newNode).Y());
						if( (*newNode).G() < (*current).G())
						{
							(*current).G((*newNode).G());
							(*current).Parent((*newNode).Parent());
							delete newNode;
						}
					}
					else
					{
						openList.push_back(newNode);
					}		
				}
			}
		}
	}

}

NodeList Board::getNeighbours(Node* parent, int heuristic)
{
	NodeList neighbours;
	int x = (*parent).X();
	int y = (*parent).Y();
	for(int i = x - 1; i<=x+1;i++)
	{
		for(int j = y - 1; j<= y +1;j++)
		{
			if(i != x || j != y)
			{
				double h = 0.0;
					if(heuristic == 0)
					{
						h = manhattenDistance(i,j,x,y);

					}
					if(heuristic == 1)
					{
						h = euclideanDistance(i,j,x,y);

					}
					else
					{
						h = manhattenDistance(i,j,x,y);
					}
					
					Node* newNode =  new Node(i,j,GetItem(i,j),h,parent);
					neighbours.push_back(newNode);
			}
		}
	}
	return neighbours;
}

//A STAR SPECIFIC METHODS

vector<Coordinates> Board::astar(Coordinates start, Coordinates goal,int heuristic) //Finds path using astar algorithm, using specified heuristic
{
	vector<Coordinates> bestPath; //the path of nodes that reach the target
	NodeList closedList; //nodes that have already been checked
	NodeList openList;//nodes that need evaluating
	Node* startNode = new Node(start.X,start.Y,GetItem(start.X,start.Y));//generates a start node
	(*startNode).G(euclideanDistance(start.X,start.Y,goal.X, goal.Y)); //probably best to set the heuristic distance from the start node, in case the algorith thinks its a good idea to try going back across it for some reason if it were 0
	openList.push_back(startNode); // puts the start node onto the open list
	generateSuccessors(start,openList,closedList,startNode,heuristic); //adds all adjacent nodes from the start node to the open list
	openList.remove(start.X,start.Y); //removes start node from open list
	closedList.addNew(start.X,start.Y,GetItem(start.X,start.Y)); // places start node in closed list
	
	bool finished = false;
	while(true) // while there are still nodes to be evaluated
	{
		Node* candidate = openList[0];
		for(int i = 0; i < openList.size();i++) 
		{
			Node& currentNode = *openList[i];
			if( currentNode.F() < (*candidate).F()) // finds the node with the lowest f value
			{
				candidate = openList[i];
			}
		}
	//	cout<<(*candidate).X()<< " " <<(*candidate).X()<<endl;
		 //if candidate is the goal then just run with it, and plop that, and all of its parents into the bestPath list. at least thats what I think its doing, could be wrong
		if((*candidate).X() == goal.X && (*candidate).Y() == goal.Y)
		{
			
			Node* node = candidate;
			Coordinates newCoord((*node).X(),(*node).Y());
			bestPath.push_back(newCoord);
			
			while(true)
			{
				if((*node).Parent() != 0)
				{
					//cout<<(*node).Parent()<<endl;
					//system("pause");
					Node newNode = *(*node).Parent(); // have to deferenence the pointer first, ill be damned if i am returning pointers.
					newCoord.X = newNode.X();
					newCoord.Y = newNode.Y();
					bestPath.push_back(newCoord);
					node = (*node).Parent();
				}
				else
				{
					
					break;
				}
			}
			break;
		}
		else
		{
			closedList.push_back(candidate);
			openList.remove( (*candidate).X(), (*candidate).Y());
			generateSuccessors(goal,openList,closedList,candidate,heuristic);
			//system("pause");
		}
		
	}

	//puts open list into bestPath list purely for testing purpouses
	//removeAllFromList(openList);
	//for(int i =0; i < openList.size();i++)
	//{
	//	cout<<i<<endl;
	//	Node newNode = *openList[i];
	//	//system("pause");
	//	bestPath.push_back(newNode);
	//}
	return bestPath;
}


//vector<Coordinates> Board::astarBidirectional(Coordinates start, Coordinates goal,int heuristic) //Finds path using astar algorithm, using specified heuristic
//{
//
//	vector<Coordinates> bestPath;
//	//the path of nodes that reach the target
//	NodeList closedListFront; //nodes that have already been checked starting from the front
//	NodeList openListFront;//nodes that need evaluating starting from the front
//	NodeList closedListBack; //nodes that have already been checked starting from the back
//	NodeList openListBack;//nodes that need evaluating starting from the back
//	Node* startNodeFront = new Node(start.X,start.Y,GetItem(start.X,start.Y));//generates a start node from the start
//	Node* startNodeBack = new Node(goal.X,goal.Y,GetItem(goal.X,goal.Y));//generates a start node from the goal
//
//	Node* joinPoint = 0;
//
//	openListFront.push_back(startNodeFront); // puts the start node onto the open list
//	generateSuccessors(start,openListFront,closedListFront,startNodeFront,heuristic); //adds all adjacent nodes from the start node to the open list
//	openListFront.remove(start.X,start.Y); //removes start node from open list
//	closedListFront.addNew(start.X,start.Y,GetItem(start.X,start.Y)); // places start node in closed list
//	
//
//	openListBack.push_back(startNodeBack); // puts the start node onto the open list
//	generateSuccessors(goal,openListBack,closedListBack,startNodeBack,heuristic); //adds all adjacent nodes from the start node to the open list
//	openListBack.remove(goal.X,goal.Y); //removes start node from open list
//	closedListBack.addNew(goal.X,goal.Y,GetItem(goal.X,goal.Y)); // places start node in closed list
//
//	bool found = false;
//
//	while(joinPoint == 0)
//	{
//		astarBidirectionalIterate(openListFront,closedListFront,goal,heuristic); //goal from the front is the goal
//		astarBidirectionalIterate(openListBack,closedListBack,start,heuristic); //goal from the back is the start
//
//		for(int i = 0; i < openListFront.size();i++)
//		{
//			if(openListBack.inList(openListFront[i]))
//			{
//				
//				//joinPoint = new Node(openListFront[i]->X(),openListFront[i]->Y(),GetItem(openListFront[i]->X(),openListFront[i]->Y()));
//				//joinPoint->Parent(openListBack.getItem(openListFront[i]->X(),openListFront[i]->Y())); //sets the parent of 
//				joinPoint = openListFront[i];
//				//cout<<openListFront[i]->X()<<" "<<openListFront[i]->Y()<<endl;
//				//cout<<openListFront.size()<<endl;
//			}
//			if(closedListBack.inList(openListFront[i]))
//			{
//				joinPoint = closedListFront[i];
//				//joinPoint->Parent(closedListBack.getItem(closedListFront[i]->X(),closedListFront[i]->Y())); //sets the parent of 
//			}
//			if(joinPoint != 0)
//			{
//				break;
//			}
//		}
//		cout<<"looping again"<<endl;
//	}
//	cout<<"excited path generation"<<endl;
//	int vectorPos = 0;
//	Node* current = joinPoint;
//	Coordinates newCoord(current->X(),current->Y());
//	vector<Coordinates> tempFront;
//
//	while(true)
//	{
//		if(current->Parent() != 0)
//				{
//					
//					//cout<<current->Parent()->X()<<" "<<current->Parent()->Y()<<endl;
//					//system("pause");
//					Node* newNode = current; // have to deferenence the pointer first, ill be damned if i am returning pointers.
//					newCoord.X = newNode->X();
//					newCoord.Y = newNode->Y();
//					tempFront.push_back(newCoord);
//					current = newNode->Parent();
//					vectorPos = vectorPos + 1;
//				}
//				else
//				{
//					
//					break;
//				}
//
//	}
//	vector<Coordinates> tempBack;
//	
//	current = openListBack.getItem(joinPoint->X(),joinPoint->Y());
//	newCoord.X = current->X();
//	newCoord.Y = current->Y();
//
//	while(true)
//	{
//		//cout<<current->Parent()<<endl;
//		if(current->Parent() != 0)
//				{
//					
//					
//					//cout<<current->Parent()->X()<<" "<<current->Parent()->Y()<<endl;
//					//system("pause");
//					Node* newNode = current; // have to deferenence the pointer first, ill be damned if i am returning pointers.
//					newCoord.X = newNode->X();
//					newCoord.Y = newNode->Y();
//					tempBack.push_back(newCoord);
//					
//					current = newNode->Parent();
//				}
//				else
//				{
//					
//					break;
//				}
//
//	}
//	
//	/*for(int i =0;i<tempFront.size();i++)
//	{
//		bestPath.push_back(tempFront[i]);
//	}*/
//	bestPath.insert(bestPath.end(),tempFront.begin(),tempFront.end());
//	bestPath.insert(bestPath.begin(),tempBack.begin(),tempBack.end());
//	/*while(tempBack.size() > 0)
//	{
//		bestPath.push_back(tempBack.pop_back());
//	}*/
//	/*for(int i =0;i<tempBack.size();i++)
//	{
//		bestPath.push_back(tempBack[i]);
//	}*/
//	
//
//	cout<<"WHY DO YOU EXIT!"<<endl;
//	cout<<"path items:"<<bestPath.size()<<endl;
//	
//	return bestPath;
//}
//
//void Board::astarBidirectionalIterate(NodeList& openList, NodeList& closedList, Coordinates goal,int heuristic)
//{
//	Node* candidate = openList[0];
//		for(int i = 0; i < openList.size();i++) 
//		{
//			Node* currentNode = openList[i];
//			if( currentNode->F() < candidate->F()) // finds the node with the lowest f value
//			{
//				candidate = openList[i];
//			}
//		}
//		if(candidate->X() != goal.X && candidate->Y() != goal.Y)
//		{
//			closedList.push_back(candidate);
//			openList.remove( (*candidate).X(), (*candidate).Y());
//			generateSuccessors(goal,openList,closedList,candidate,heuristic);
//		}
//}


//JumpPoint specific Methods

//vector<Coordinates> Board::jumpPoint(Coordinates start, Coordinates goal,int heuristic)
//{
//	vector<Coordinates> bestPath;
//	NodeList openList;
//	NodeList closedList;
//	Node* current = new Node(start.X,start.Y,GetItem(start.X,start.Y));
//	bool found = false;
//	while (found == false)
//	{
//		NodeList successors = jumpPointSuccessors(start,goal,current,heuristic);
//		for(int i =0; i < successors.size();i++)
//		{
//			if(openList.inList(successors[i]) == true)
//			{
//				
//				
//			}
//			if(closedList.inList(successors[i]))
//			{
//			}
//		}
//	}
//	return bestPath;
//}
//
//NodeList Board::jumpPointSuccessors(Coordinates start, Coordinates goal, Node* parent,int heuristic)
//{
//	Coordinates p(0,0);
//	p.X = parent->X();
//	p.Y = parent->Y();
//	NodeList successors;
//	NodeList neighbours = getNeighbours(parent,heuristic);
//	for(int i = 0; i < neighbours.size();i++)
//	{
//		Coordinates direction(0,0);
//		direction.X = neighbours[i]->X() - parent->X();  //x direction
//		direction.Y = neighbours[i]->Y() - parent->Y();  //y direction
//	
//		Node* jumpPoint = jump(p,direction,start,goal,parent,heuristic);
//		//If a jump point has been found, push to the back of the list
//		if(jumpPoint != 0) {successors.push_back(jumpPoint);}
//	}
//	return successors;
//}
//
//Node* Board::jump(Coordinates current, Coordinates direction,Coordinates start, Coordinates end, Node* parent,int heuristic)
//{
//	Node* nextNode = new Node(); //Node that will hold the jump point, if one if found, initialise to 0 (NULL) initally
//	Coordinates next(current.X + direction.X,current.Y + direction.Y); //position of new node to consider
//	if(GetItem(next.X,next.Y) == 0) { return 0; } //if impassible, skip it
//
//	if( (next.X == end.X) && (next.Y = end.Y) ) //if it is the end point, return it
//	{			
//		return new Node(next.X,next.Y,GetItem(next.X,next.Y),0,parent); //H is set to 0, because if its the end then who really cares what its h value is?
//	}
//
//	if( (direction.X != 0) && (direction.Y !=0) ) //Diagonal case
//	{
//		Coordinates diagUp(direction.X,0);
//		Coordinates diagDown(0,direction.Y);
//		if(true) //diagonal forced neighbour check
//		{
//			return new Node(next.X,next.Y,GetItem(next.X,next.Y),0,parent);
//		}
//		if( (jump(next,diagUp,start,end,nextNode,heuristic) != 0) || (jump(next,diagDown,start,end,nextNode,heuristic) != 0) ) 
//		{
//			return new Node(next.X,next.Y,GetItem(next.X,next.Y),0,parent);
//		}
//	}
//	else
//	{
//		if(direction.X != 0) //Horizontal case
//		{
//			if(true)//horizontal forced neighbour check
//			{
//				return new Node(next.X,next.Y,GetItem(next.X,next.Y),0,parent);
//			}
//		}
//		else
//		{
//			if(true)//vertical forced neighbour check
//			{
//				new Node(next.X,next.Y,GetItem(next.X,next.Y),0,parent);
//			}
//		}
//	}
//	return jump(next,direction,start,end,parent,heuristic);
//}
//
//bool Board::diagonalForcedNeighbour(Coordinates current,Coordinates direction)
//{
//	
//}
//bool Board::horizontalForcedNeighbour(Coordinates current,Coordinates direction)
//{
//}
//bool Board::verticalForcedNeighbour(Coordinates current, Coordinates direction)
//{
//}