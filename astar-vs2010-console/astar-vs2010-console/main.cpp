#include <iostream>
#include "Board.h"
#include "Files.h"
#include <vector>
//#include "PathFinder.h"
using namespace std;


int main()
{
	//cout<<manhattenDistance(5,2,2,5)<<endl;
	//cout<<euclideanDistance(5,2,2,5)<<endl;
	Board newBoard(11,8,LoadTextFile("test53.txt",11,8));
	////cout<<newBoard.GetItem(2,2)<<endl;
	////cout<<newBoard.GetItem(2,1)<<endl;
	//vector<Node> path = AStar(1,1,9,5,newBoard);
	//for(int i =0; i < path.size();i++)
	//{
	//	cout<<path[i].X()<<" "<<path[i].Y()<<"  "<<path[i].F()<<endl;
	//}
	//system("pause");
	cout<<"A* algorithms with different heuristics"<<endl;
	vector<Coordinates> pathM = newBoard.findPath(1,1,9,5,0,0);
	cout<<"Manhatten A*"<<endl;
	for(int i =0; i < pathM.size();i++)
	{
		cout<<pathM[i].X<<" "<<pathM[i].Y<<endl;
	}
	cout<<"Euclidean A*"<<endl;
	vector<Coordinates> pathE = newBoard.findPath(1,1,9,5,0,1);
	for(int i =0; i < pathE.size();i++)
	{
		cout<<pathE[i].X<<" "<<pathE[i].Y<<endl;
	}

	cout<<"Chebyshev A*"<<endl;
	vector<Coordinates> pathC = newBoard.findPath(1,1,9,5,0,2);
	for(int i =0; i < pathC.size();i++)
	{
		cout<<pathC[i].X<<" "<<pathC[i].Y<<endl;
	}
	system("pause");	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
	//vector<int,int> vect;
	//vect[0] = 1, 1;
	return 0;
}