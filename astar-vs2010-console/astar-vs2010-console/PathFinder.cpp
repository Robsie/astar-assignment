#include "PathFinder.h"


vector<Node> AStar(int startX, int startY, int goalX, int goalY, Board board)
{
	vector<Node> bestPath;//the path of nodes that reach the target
	vector<Node*> closedList; //nodes that have already been checked
	vector<Node*> openList;//nodes that need evaluating
	Node* startNode = new Node(startX,startY,board.GetItem(startX,startY));//generates a start node
	(*startNode).G(euclideanDistance(startX,startY,goalX, goalY)); //probably best to set the heuristic distance from the start node, in case the algorith thinks its a good idea to try going back across it for some reason if it were 0
	openList.push_back(startNode); // puts the start node onto the open list
	generateSuccessors(goalX,goalY,openList,closedList,board,startNode); //adds all adjacent nodes from the start node to the open list
	removeNodeFromList(startX,startY,openList); //removes start node from open list
	addNodeToList(startX,startY,closedList,board); // places start node in closed list
	
	bool finished = false;
	while(true) // while there are still nodes to be evaluated
	{
		Node* candidate = openList[0];
		for(int i = 0; i < openList.size();i++) 
		{
			Node& currentNode = *openList[i];
			if( currentNode.F() < (*candidate).F() && (currentNode.X() != (*startNode).X() && currentNode.Y() != (*startNode).Y()) ) // finds the node with the lowest f value
			{
				candidate = openList[i];
			}
		}
	//	cout<<(*candidate).X()<< " " <<(*candidate).X()<<endl;
		 //if candidate is the goal then just run with it, and plop that, and all of its parents into the bestPath list. at least thats what I think its doing, could be wrong
		if((*candidate).X() == goalX && (*candidate).Y() == goalY)
		{
			
			Node* node = candidate;
			bestPath.push_back( (*node));
			while(true)
			{
				if((*node).Parent() != 0)
				{
					//cout<<(*node).Parent()<<endl;
					//system("pause");
					Node newNode = *(*node).Parent(); // have to deferenence the pointer first, ill be damned if i am returning pointers.
					bestPath.push_back(newNode);
					node = (*node).Parent();
				}
				else
				{
					
					break;
				}
			}
			break;
		}
		else
		{
			closedList.push_back(candidate);
			removeNodeFromList( (*candidate).X(), (*candidate).Y(),openList);
			generateSuccessors(goalX,goalY,openList,closedList,board,candidate);
			//system("pause");
		}
		
	}

	//puts open list into bestPath list purely for testing purpouses
	//removeAllFromList(openList);
	//for(int i =0; i < openList.size();i++)
	//{
	//	cout<<i<<endl;
	//	Node newNode = *openList[i];
	//	//system("pause");
	//	bestPath.push_back(newNode);
	//}
	return bestPath;
}

void addNodeToList(int x, int y, vector<Node*>& list,Board& board)
{
	Node* newNode = new Node(x,y,board.GetItem(x,y));
	list.push_back(newNode);
}

void generateSuccessors(int goalX,int goalY,vector<Node*>& openList, vector<Node*> closedList, Board& board,Node* parent)
{
	int x = (*parent).X();
	int y = (*parent).Y();
	for(int i = x - 1; i<=x+1;i++)
	{
		for(int j = y - 1; j<= y +1;j++)
		{
			if(i != x || j != y)
			{
				if(board.GetItem(i,j) != 0 && i >=0 && j >=0 )
				{
				//	cout<<"instance: "<<i<<" "<<j<<endl;
					//double h = manhattenDistance(i,j,goalX,goalY);
					double h = euclideanDistance(i,j,goalX,goalY);
					Node* newNode =  new Node(i,j,board.GetItem(i,j),h,parent);
					if(inList(closedList,newNode)) // if its already in the closed list
					{
						//update the existing one to the new parent and g value
						Node* current = getNode(closedList,(*newNode).X(),(*newNode).Y());
						if( (*newNode).G() < (*current).G())
						{
							(*current).G((*newNode).G());
							(*current).Parent((*newNode).Parent());
							delete newNode;
						}
					}
					else if(inList(openList,newNode)) // if its already in the open list
					{
						//update the existing one to the new parent and p value
						Node* current = getNode(openList,(*newNode).X(),(*newNode).Y());
						if( (*newNode).G() < (*current).G())
						{
							(*current).G((*newNode).G());
							(*current).Parent((*newNode).Parent());
							delete newNode;
						}
					}
					else
					{
						openList.push_back(newNode);
					}		
				}
			}
		}
	}
}

void removeNodeFromList(int x, int y,vector<Node*>& list)
{
	for(int i =0; i < list.size(); i++)
	{
		Node& node = *list[i];
		if(node.X() == x && node.Y() == y)
		{
			//delete list[i];
			list.erase(list.begin() + i);
		}
	}
}

bool inList(vector<Node*> list,Node* node)
{
	for(int i = 0; i < list.size();i++)
	{
		if((*list[i]).X() == (*node).X() && (*list[i]).Y() == (*node).Y()) {return true;}
	}
	return false;
}

Node* getNode(vector<Node*> list, int x, int y)
{
		for(int i = 0; i < list.size();i++)
		{
			if( (*list[i]).X() == x && (*list[i]).Y() == y)
			{
				return list[i];
			}
		}
		return 0;
}

void removeAllFromList(vector<Node*> list)
{
	for( vector<Node*>::iterator i = list.begin(); i != list.end(); ++i )
	{
		delete *i;
	}
}

double getScore(int currentX, int currentY, int goalX,int goalY)
{
	double score = 0.0;
	return score;
}

double euclideanDistance(int currentX, int currentY,int goalX, int goalY) //uses fancy geometery to figure out the distance to the goal, no idea how this works at all, geometry is for squares.
{
	return sqrt(pow(currentX-goalX,2.0) + pow(currentY-goalY,2.0));
}

double manhattenDistance(int currentX, int currentY, int goalX, int goalY) //gets distance up and distance across from the goal, I think. Maybe. We shall see.
{
	return (abs(currentX-goalX) + abs(currentY-goalY));
}



