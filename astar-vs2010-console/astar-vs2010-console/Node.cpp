#include "Node.h"

//private



//public
//constructors
Node::Node(int x, int y, int cost)
{
	_x = x;
	_y = y;
	/*_pX = -1;
	_pY = -1;*/
	_cost = cost;
	_f = 0.0;
	_h = 0.0;
	_g = 0.0;
	_parent = 0;
}

Node::Node(int x, int y, int cost, double h, Node* parent)
{
	_x = x;
	_y = y;
	_cost = cost;
	_parent = parent;
	_h = h;
	_g = (*parent).G() + cost;
	_f = _h + _g;
	;
}

Node::Node()
{
	_x = 0;
	_y = 0;
	_cost = 0;
}

//getters and setters (properties)
int Node::X() const
{
	return _x;
}

void Node::X(int x)
{
	_x = x;
}

int Node::Y() const
{
	return _y;
}

void Node::Y(int y)
{
	_y = y;
}

Node* Node::Parent() const
{
	return _parent;
}
void Node::Parent(Node* parent)
{
	_parent = parent;
}

//int Node::PX() const
//{
//	return _pX;
//}
//
//void Node::PX(int px)
//{
//	_pX = px;
//}
//
//int Node::PY() const
//{
//	return _pY;
//}
//
//void Node::PY(int py)
//{
//	_pY = py;
//}

double Node::F() const
{
	return _h + _g;
}

void Node::F(double f)
{
	_f = f;
}

double Node::H() const
{
	return _h;
}

void Node::H(double h) 
{
	_h = h;
}

double Node::G() const
{
	return _g;
}

void Node::G(double g)
{
	_g = g;
}

void Node::Cost(int cost)
{
	_cost = cost;
}

int Node::Cost() const
{
	return _cost;
}

//operators
Node& Node::operator=(const Node& n)
{
	if(this != &n)
	{
		_x = n.X();
		_y = n.Y();
		_cost = n.Cost();
	}
	return *this;

}