/********************************************************************************
** Form generated from reading UI file 'gridwindow.ui'
**
** Created: Mon 29. Apr 14:00:12 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRIDWINDOW_H
#define UI_GRIDWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGraphicsView>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GridWindow
{
public:
    QWidget *centralWidget;
    QComboBox *comboBoxAlgorithm;
    QLineEdit *textBoxGoalX;
    QGraphicsView *graphicsViewGrid;
    QLabel *labelY_2;
    QPushButton *buttonPath;
    QLabel *labelGoal;
    QLabel *labelAlgorithm;
    QLabel *labelStart;
    QLabel *labelX;
    QLabel *labelX_2;
    QLabel *labelY;
    QLineEdit *textBoxStartX;
    QLineEdit *textBoxGoalY;
    QLineEdit *textBoxStartY;
    QComboBox *comboBoxHeuristic;
    QLabel *labelHeuristic;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *GridWindow)
    {
        if (GridWindow->objectName().isEmpty())
            GridWindow->setObjectName(QString::fromUtf8("GridWindow"));
        GridWindow->resize(520, 590);
        centralWidget = new QWidget(GridWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        comboBoxAlgorithm = new QComboBox(centralWidget);
        comboBoxAlgorithm->setObjectName(QString::fromUtf8("comboBoxAlgorithm"));
        comboBoxAlgorithm->setGeometry(QRect(100, 440, 100, 22));
        textBoxGoalX = new QLineEdit(centralWidget);
        textBoxGoalX->setObjectName(QString::fromUtf8("textBoxGoalX"));
        textBoxGoalX->setGeometry(QRect(120, 500, 20, 20));
        graphicsViewGrid = new QGraphicsView(centralWidget);
        graphicsViewGrid->setObjectName(QString::fromUtf8("graphicsViewGrid"));
        graphicsViewGrid->setGeometry(QRect(50, 10, 420, 420));
        labelY_2 = new QLabel(centralWidget);
        labelY_2->setObjectName(QString::fromUtf8("labelY_2"));
        labelY_2->setGeometry(QRect(160, 500, 16, 20));
        buttonPath = new QPushButton(centralWidget);
        buttonPath->setObjectName(QString::fromUtf8("buttonPath"));
        buttonPath->setGeometry(QRect(260, 500, 100, 22));
        labelGoal = new QLabel(centralWidget);
        labelGoal->setObjectName(QString::fromUtf8("labelGoal"));
        labelGoal->setGeometry(QRect(50, 500, 46, 13));
        labelAlgorithm = new QLabel(centralWidget);
        labelAlgorithm->setObjectName(QString::fromUtf8("labelAlgorithm"));
        labelAlgorithm->setGeometry(QRect(50, 440, 51, 16));
        labelStart = new QLabel(centralWidget);
        labelStart->setObjectName(QString::fromUtf8("labelStart"));
        labelStart->setGeometry(QRect(50, 470, 46, 13));
        labelX = new QLabel(centralWidget);
        labelX->setObjectName(QString::fromUtf8("labelX"));
        labelX->setGeometry(QRect(100, 470, 16, 20));
        labelX_2 = new QLabel(centralWidget);
        labelX_2->setObjectName(QString::fromUtf8("labelX_2"));
        labelX_2->setGeometry(QRect(100, 500, 16, 20));
        labelY = new QLabel(centralWidget);
        labelY->setObjectName(QString::fromUtf8("labelY"));
        labelY->setGeometry(QRect(160, 470, 16, 20));
        textBoxStartX = new QLineEdit(centralWidget);
        textBoxStartX->setObjectName(QString::fromUtf8("textBoxStartX"));
        textBoxStartX->setGeometry(QRect(120, 470, 20, 20));
        textBoxGoalY = new QLineEdit(centralWidget);
        textBoxGoalY->setObjectName(QString::fromUtf8("textBoxGoalY"));
        textBoxGoalY->setGeometry(QRect(180, 500, 20, 20));
        textBoxStartY = new QLineEdit(centralWidget);
        textBoxStartY->setObjectName(QString::fromUtf8("textBoxStartY"));
        textBoxStartY->setGeometry(QRect(180, 470, 20, 20));
        comboBoxHeuristic = new QComboBox(centralWidget);
        comboBoxHeuristic->setObjectName(QString::fromUtf8("comboBoxHeuristic"));
        comboBoxHeuristic->setGeometry(QRect(260, 440, 100, 22));
        labelHeuristic = new QLabel(centralWidget);
        labelHeuristic->setObjectName(QString::fromUtf8("labelHeuristic"));
        labelHeuristic->setGeometry(QRect(210, 440, 51, 16));
        GridWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(GridWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 520, 20));
        GridWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(GridWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        GridWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(GridWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        GridWindow->setStatusBar(statusBar);

        retranslateUi(GridWindow);

        QMetaObject::connectSlotsByName(GridWindow);
    } // setupUi

    void retranslateUi(QMainWindow *GridWindow)
    {
        GridWindow->setWindowTitle(QApplication::translate("GridWindow", "GridWindow", 0, QApplication::UnicodeUTF8));
        comboBoxAlgorithm->clear();
        comboBoxAlgorithm->insertItems(0, QStringList()
         << QApplication::translate("GridWindow", "astar", 0, QApplication::UnicodeUTF8)
        );
        labelY_2->setText(QApplication::translate("GridWindow", "Y:", 0, QApplication::UnicodeUTF8));
        buttonPath->setText(QApplication::translate("GridWindow", "Path..", 0, QApplication::UnicodeUTF8));
        labelGoal->setText(QApplication::translate("GridWindow", "Goal:", 0, QApplication::UnicodeUTF8));
        labelAlgorithm->setText(QApplication::translate("GridWindow", "Algorithm:", 0, QApplication::UnicodeUTF8));
        labelStart->setText(QApplication::translate("GridWindow", "Start:", 0, QApplication::UnicodeUTF8));
        labelX->setText(QApplication::translate("GridWindow", "X:", 0, QApplication::UnicodeUTF8));
        labelX_2->setText(QApplication::translate("GridWindow", "X:", 0, QApplication::UnicodeUTF8));
        labelY->setText(QApplication::translate("GridWindow", "Y:", 0, QApplication::UnicodeUTF8));
        comboBoxHeuristic->clear();
        comboBoxHeuristic->insertItems(0, QStringList()
         << QApplication::translate("GridWindow", "manhatten", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("GridWindow", "euclidean", 0, QApplication::UnicodeUTF8)
        );
        labelHeuristic->setText(QApplication::translate("GridWindow", "Heuristic:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GridWindow: public Ui_GridWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRIDWINDOW_H
