/********************************************************************************
** Form generated from reading UI file 'gridwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRIDWINDOW_H
#define UI_GRIDWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GridWindow
{
public:
    QWidget *centralWidget;
    QLineEdit *textBoxGoalX;
    QLineEdit *textBoxStartX;
    QComboBox *comboBoxHeuristic;
    QComboBox *comboBoxAlgorithm;
    QLineEdit *textBoxStartY;
    QLabel *labelStart;
    QLabel *labelX_2;
    QGraphicsView *graphicsViewGrid;
    QLabel *labelAlgorithm;
    QLabel *labelGoal;
    QLineEdit *textBoxGoalY;
    QLabel *labelHeuristic;
    QLabel *labelY_2;
    QLabel *labelY;
    QLabel *labelX;
    QPushButton *buttonPath;
    QPushButton *pushButtonOpenFile;
    QRadioButton *radioButtonFullVisualisation;
    QLineEdit *textBoxColumns;
    QLineEdit *textBoxRows;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *GridWindow)
    {
        if (GridWindow->objectName().isEmpty())
            GridWindow->setObjectName(QStringLiteral("GridWindow"));
        GridWindow->resize(520, 581);
        centralWidget = new QWidget(GridWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        textBoxGoalX = new QLineEdit(centralWidget);
        textBoxGoalX->setObjectName(QStringLiteral("textBoxGoalX"));
        textBoxGoalX->setGeometry(QRect(120, 500, 20, 20));
        textBoxStartX = new QLineEdit(centralWidget);
        textBoxStartX->setObjectName(QStringLiteral("textBoxStartX"));
        textBoxStartX->setGeometry(QRect(120, 470, 20, 20));
        comboBoxHeuristic = new QComboBox(centralWidget);
        comboBoxHeuristic->setObjectName(QStringLiteral("comboBoxHeuristic"));
        comboBoxHeuristic->setGeometry(QRect(260, 440, 100, 22));
        comboBoxAlgorithm = new QComboBox(centralWidget);
        comboBoxAlgorithm->setObjectName(QStringLiteral("comboBoxAlgorithm"));
        comboBoxAlgorithm->setGeometry(QRect(100, 440, 100, 22));
        textBoxStartY = new QLineEdit(centralWidget);
        textBoxStartY->setObjectName(QStringLiteral("textBoxStartY"));
        textBoxStartY->setGeometry(QRect(180, 470, 20, 20));
        labelStart = new QLabel(centralWidget);
        labelStart->setObjectName(QStringLiteral("labelStart"));
        labelStart->setGeometry(QRect(50, 470, 46, 13));
        labelX_2 = new QLabel(centralWidget);
        labelX_2->setObjectName(QStringLiteral("labelX_2"));
        labelX_2->setGeometry(QRect(100, 500, 16, 20));
        graphicsViewGrid = new QGraphicsView(centralWidget);
        graphicsViewGrid->setObjectName(QStringLiteral("graphicsViewGrid"));
        graphicsViewGrid->setGeometry(QRect(50, 10, 420, 420));
        labelAlgorithm = new QLabel(centralWidget);
        labelAlgorithm->setObjectName(QStringLiteral("labelAlgorithm"));
        labelAlgorithm->setGeometry(QRect(50, 440, 51, 16));
        labelGoal = new QLabel(centralWidget);
        labelGoal->setObjectName(QStringLiteral("labelGoal"));
        labelGoal->setGeometry(QRect(50, 500, 46, 13));
        textBoxGoalY = new QLineEdit(centralWidget);
        textBoxGoalY->setObjectName(QStringLiteral("textBoxGoalY"));
        textBoxGoalY->setGeometry(QRect(180, 500, 20, 20));
        labelHeuristic = new QLabel(centralWidget);
        labelHeuristic->setObjectName(QStringLiteral("labelHeuristic"));
        labelHeuristic->setGeometry(QRect(210, 440, 51, 16));
        labelY_2 = new QLabel(centralWidget);
        labelY_2->setObjectName(QStringLiteral("labelY_2"));
        labelY_2->setGeometry(QRect(160, 500, 16, 20));
        labelY = new QLabel(centralWidget);
        labelY->setObjectName(QStringLiteral("labelY"));
        labelY->setGeometry(QRect(160, 470, 16, 20));
        labelX = new QLabel(centralWidget);
        labelX->setObjectName(QStringLiteral("labelX"));
        labelX->setGeometry(QRect(100, 470, 16, 20));
        buttonPath = new QPushButton(centralWidget);
        buttonPath->setObjectName(QStringLiteral("buttonPath"));
        buttonPath->setGeometry(QRect(260, 500, 100, 22));
        pushButtonOpenFile = new QPushButton(centralWidget);
        pushButtonOpenFile->setObjectName(QStringLiteral("pushButtonOpenFile"));
        pushButtonOpenFile->setGeometry(QRect(390, 500, 75, 23));
        radioButtonFullVisualisation = new QRadioButton(centralWidget);
        radioButtonFullVisualisation->setObjectName(QStringLiteral("radioButtonFullVisualisation"));
        radioButtonFullVisualisation->setGeometry(QRect(260, 470, 101, 17));
        textBoxColumns = new QLineEdit(centralWidget);
        textBoxColumns->setObjectName(QStringLiteral("textBoxColumns"));
        textBoxColumns->setGeometry(QRect(440, 470, 20, 20));
        textBoxRows = new QLineEdit(centralWidget);
        textBoxRows->setObjectName(QStringLiteral("textBoxRows"));
        textBoxRows->setGeometry(QRect(390, 470, 20, 20));
        GridWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(GridWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 520, 21));
        GridWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(GridWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        GridWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(GridWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        GridWindow->setStatusBar(statusBar);

        retranslateUi(GridWindow);

        QMetaObject::connectSlotsByName(GridWindow);
    } // setupUi

    void retranslateUi(QMainWindow *GridWindow)
    {
        GridWindow->setWindowTitle(QApplication::translate("GridWindow", "GridWindow", 0));
        comboBoxHeuristic->clear();
        comboBoxHeuristic->insertItems(0, QStringList()
         << QApplication::translate("GridWindow", "manhatten", 0)
         << QApplication::translate("GridWindow", "chebyshev", 0)
         << QApplication::translate("GridWindow", "euclidean", 0)
        );
        comboBoxAlgorithm->clear();
        comboBoxAlgorithm->insertItems(0, QStringList()
         << QApplication::translate("GridWindow", "astar", 0)
        );
        labelStart->setText(QApplication::translate("GridWindow", "Start:", 0));
        labelX_2->setText(QApplication::translate("GridWindow", "X:", 0));
        labelAlgorithm->setText(QApplication::translate("GridWindow", "Algorithm:", 0));
        labelGoal->setText(QApplication::translate("GridWindow", "Goal:", 0));
        labelHeuristic->setText(QApplication::translate("GridWindow", "Heuristic:", 0));
        labelY_2->setText(QApplication::translate("GridWindow", "Y:", 0));
        labelY->setText(QApplication::translate("GridWindow", "Y:", 0));
        labelX->setText(QApplication::translate("GridWindow", "X:", 0));
        buttonPath->setText(QApplication::translate("GridWindow", "Path..", 0));
        pushButtonOpenFile->setText(QApplication::translate("GridWindow", "Select File", 0));
        radioButtonFullVisualisation->setText(QApplication::translate("GridWindow", "Full Visualisation", 0));
        textBoxColumns->setText(QApplication::translate("GridWindow", "20", 0));
        textBoxRows->setText(QApplication::translate("GridWindow", "20", 0));
    } // retranslateUi

};

namespace Ui {
    class GridWindow: public Ui_GridWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRIDWINDOW_H
