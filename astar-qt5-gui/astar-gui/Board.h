#pragma
#ifndef Board_H
#define Board_H
#include <cmath>
#include <ctime>
#include <string>
#include <vector>
#include <windows.h>
#include <iostream>

#include <QGraphicsView>

#include "TwoDimensionalArray.h"
#include "TwoDimensionalArray.h"
#include "Node.h"
#include "NodeList.h"

using namespace std;

struct Coordinates
{
	int X;
	int Y;
	Coordinates(int x, int y)
	{
		X = x;
		Y = y;
	}
};

class Board : public TwoDimensionalArray<int>
{
private:
	//HEURISTIC METHODS
	double euclideanDistance(int currentX, int currentY,int goalX, int goalY);
	double euclideanDistance(Coordinates current,Coordinates goal);
	double manhattenDistance(int currentX, int currentY,int goalX, int goalY);
	double manhattenDistance(Coordinates current,Coordinates goal);
	double chebyshevDistance(int currentX, int currentY,int goalX, int goalY);
	double chebyshevDistance(Coordinates current,Coordinates goal);
	// Private PATH FINDING METHODS
	void generateSuccessors(Coordinates goal, NodeList& openList, NodeList& closedList, Node* parent,int heuristic);

	NodeList getNeighbours(Node* parent, int heurstic);
	
	// Private A STAR SPECIFIC METHODS
	vector<Coordinates> astar(Coordinates start, Coordinates goal,int heuristic); //Finds path using astar algorithm, using specified heuristic
    vector<Coordinates> astarVisualise(Coordinates start, Coordinates goal,int heuristic,QGraphicsView* view); //Finds path using astar algorithm, using specified heuristic, passes a qgraphics view to show output as it works
    //Private Visualisation Methods
    void visualiseFinal(Coordinates start, Coordinates goal, vector<Coordinates> path, NodeList& openList, NodeList& closedList,QGraphicsView* view);


public:
	Board(int r, int c);
	Board(int r, int c, int* d);
	//PUBLIC PATH FINDING METHODS
	vector<Coordinates> findPath(int startX, int startY, int goalX, int goalY,int type, int heuristic); // find path using specified algorithm and heuristic type passing ints
	vector<Coordinates> findPath(Coordinates start, Coordinates goal, int type,int heuristic); // find path using specified algorithm and heuristic type passing Coordinates
    //takes an extra argument of QGraphicsView which the methods will output to to display changes
//    vector<Coordinates> findPathVisualise(int startX, int startY, int goalX, int goalY,NodeList& openList, NodeList& closedList, int type, int heuristic); // find path using specified algorithm and heuristic type passing ints
//    vector<Coordinates> findPathVisualise(Coordinates start, Coordinates goal,NodeList& openList, NodeList& closedList, int type,int heuristic); // find path using specified algorithm and heuristic type passing Coordinates

    vector<Coordinates> findPathVisualise(int startX, int startY, int goalX, int goalY,int type, int heuristic,QGraphicsView* view); // find path using specified algorithm and heuristic type passing ints
    vector<Coordinates> findPathVisualise(Coordinates start, Coordinates goal,int type,int heuristic,QGraphicsView* view); // find path using specified algorithm and heuristic type passing Coordinates

};

#endif
