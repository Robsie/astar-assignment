#ifndef GRIDWINDOW_H
#define GRIDWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDialog>
#include <QFileDialog>
#include <QGraphicsView>
#include "Board.h"
#include "Files.h"

namespace Ui {
class GridWindow;
}

class GridWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit GridWindow(QWidget *parent = 0);
    ~GridWindow();

private slots:
    void on_buttonPath_clicked();

    void on_pushButtonOpenFile_clicked();

private:
     Ui::GridWindow *ui;
     std::string _fileName = "board1.txt";
     void setFileName(std::string);
     void initiateGrid();
     void displayGrid(Board board,vector<Coordinates> path, Coordinates start, Coordinates goal);
     void displayGridFull(Board board ,Coordinates start, Coordinates goal, vector<Coordinates> path, NodeList& openList, NodeList& closedList);
     void path(Board board,Coordinates start, Coordinates goal, int algorithm, int heuristic);
     void pathVisualise(Board board,Coordinates start, Coordinates goal,int algorithm, int heuristic,QGraphicsView* view);
     void passGridView(QGraphicsView* view);
};

#endif // GRIDWINDOW_H
