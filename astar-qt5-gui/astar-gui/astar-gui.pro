#-------------------------------------------------
#
# Project created by QtCreator 2013-04-29T14:57:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = astar-gui
TEMPLATE = app


SOURCES += main.cpp\
        gridwindow.cpp \
    NodeList.cpp \
    Node.cpp \
    Files.cpp \
    Board.cpp

HEADERS  += gridwindow.h \
    TwoDimensionalArray.h \
    NodeList.h \
    Node.h \
    Files.h \
    Board.h

FORMS    += gridwindow.ui

