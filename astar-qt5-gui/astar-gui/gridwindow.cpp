#include "gridwindow.h"
#include "ui_gridwindow.h"

GridWindow::GridWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GridWindow)
{
    ui->setupUi(this);
    initiateGrid();
}

GridWindow::~GridWindow()
{
    delete ui;
}

void GridWindow::setFileName(std::string fname)
{
    std::stringstream ss;

      for (int i = 0; i < fname.length(); ++i) {
         if (fname[i] == '\\') {
           ss << '\\\\';
         }
         else {
           ss << fname[i];
         }
      }
      _fileName = ss.str();
}

void GridWindow::initiateGrid()
{
    int r = this->ui->textBoxRows->text().toInt();
    int c = this->ui->textBoxColumns->text().toInt();
    if(r != 0 && c != 0)
    {
     Board board(r,c,LoadTextFile(_fileName,r,c));
     int cellSize =0;
     if(board.Rows()<=20 || board.Columns() <=20)
     {
         cellSize = 20;
     }
     else if(board.Rows()<=40 || board.Columns() <=40)
     {
         cellSize = 10;
     }
     else
     {
         cellSize = 5;
     }
     QGraphicsScene* scene = new QGraphicsScene;
     for(int i =0;i<board.Rows();i++)
     {
         for(int j =0; j<board.Columns();j++)
         {
             if(board.GetItem(i,j) == 0)
             {
                  scene->addRect(QRectF(cellSize*(i), cellSize*(j), cellSize, cellSize), QPen(Qt::black), QBrush(Qt::black));
             }
             else if (board.GetItem(i,j) == 1)
             {
                    scene->addRect(QRectF(cellSize*(i), cellSize*(j), cellSize, cellSize), QPen(Qt::black), QBrush(Qt::white));
             }
             else if (board.GetItem(i,j) >1)
             {
                 int gColor = 210 - (board.GetItem(i,j) * 5);
                 if(gColor < 50){gColor = 50;}
                  scene->addRect(QRectF(cellSize*(i), cellSize*(j), cellSize, cellSize), QPen(Qt::black), QBrush(QColor(gColor,gColor,gColor)));
             }

         }
     }
      this->ui->graphicsViewGrid->setScene(scene);
    }

}

void GridWindow::displayGrid(Board board, vector<Coordinates> path, Coordinates start, Coordinates goal)
{
    int cellSize =0;
    if(board.Rows()<=20 || board.Columns() <=20)
    {
        cellSize = 20;
    }
    else if(board.Rows()<=40 || board.Columns() <=40)
    {
        cellSize = 10;
    }
    else
    {
        cellSize = 5;
    }

    this->ui->graphicsViewGrid->scene()->clear();
     QGraphicsScene* scene = new QGraphicsScene;
     if(start.X < board.Rows() && start.Y < board.Columns() && goal.X < board.Rows() && goal.Y < board.Columns())
     {
     for(int i =0;i<board.Rows();i++)
     {
         for(int j =0; j<board.Columns();j++)
         {
             if(board.GetItem(i,j) == 0)
             {
                  scene->addRect(QRectF(cellSize*(i), cellSize*(j), cellSize, cellSize), QPen(Qt::black), QBrush(Qt::black));
             }
             else if(board.GetItem(i,j) == 1)
             {
                  scene->addRect(QRectF(cellSize*(i), cellSize*(j), cellSize, cellSize), QPen(Qt::black), QBrush(Qt::white));
             }
             else if (board.GetItem(i,j) >1)
             {
                 int gColor = 210 - (board.GetItem(i,j) * 5);
                 if(gColor < 50){gColor = 50;} //To stop it turning invisibly black
                 scene->addRect(QRectF(cellSize*(i), cellSize*(j), cellSize, cellSize), QPen(Qt::black), QBrush(QColor(gColor,gColor,gColor)));
             }

         }
     }

     for(int i = 0; i < path.size();i++) //Colour in the path
     {
         scene->addRect(QRectF(cellSize*(path[i].X), cellSize*(path[i].Y), cellSize, cellSize), QPen(Qt::black), QBrush(Qt::blue));
     }
      scene->addRect(QRectF(cellSize*(start.X), cellSize*(start.Y), cellSize, cellSize), QPen(Qt::black), QBrush(Qt::green)); //Colour start node
      scene->addRect(QRectF(cellSize*(goal.X), cellSize*(goal.Y), cellSize, cellSize), QPen(Qt::black), QBrush(Qt::red)); //Colour end node

     // Fit the view in the scene's bounding rect
     this->ui->graphicsViewGrid->setScene(scene);
     }
     else
     {
         QMessageBox msgBox;
         msgBox.setWindowTitle("Error");
         msgBox.setInformativeText("Coordinates out of range, please try again");
         msgBox.exec();
     }
}

void GridWindow::displayGridFull(Board board ,Coordinates start, Coordinates goal, vector<Coordinates> path, NodeList& openList, NodeList& closedList)
{
}

 void GridWindow::path(Board board,Coordinates start, Coordinates goal, int algorithm, int heuristic)
 {
     vector<Coordinates> path = board.findPath(start,goal,0,heuristic);
     displayGrid(board,path,start,goal);
 }

void GridWindow::pathVisualise(Board board,Coordinates start, Coordinates goal,int algorithm, int heuristic,QGraphicsView* view)
 {
   // QGraphicsView& view = qobject_cast<QGraphicsView>(graphicsGridView);
  //  NodeList openList;
   // NodeList closedList;

    vector<Coordinates> path = board.findPathVisualise(start,goal,0,heuristic,view);
    //displayGridFull(board,start,goal,path,openList,closedList);
 }

void GridWindow::passGridView(QGraphicsView* view)
{
     QGraphicsScene* scene = new QGraphicsScene;
     scene->addRect(QRectF(20*(1), 20*(1), 20, 20), QPen(Qt::black), QBrush(Qt::yellow)); //Colour start node
     scene->addRect(QRectF(20*(1), 20*(1), 20, 20), QPen(Qt::black), QBrush(Qt::yellow)); //Colour end node
     view->setScene(scene);
}

//SLOTS
void GridWindow::on_buttonPath_clicked()
{
    try
    {

    int r = this->ui->textBoxRows->text().toInt();
    int c = this->ui->textBoxColumns->text().toInt();
    Coordinates start(this->ui->textBoxStartX->text().toInt(),this->ui->textBoxStartY->text().toInt());
    Coordinates goal(this->ui->textBoxGoalX->text().toInt(),this->ui->textBoxGoalY->text().toInt());
    if(r != 0 && c != 0 && this->ui->textBoxGoalX->text() != "" && this->ui->textBoxGoalY->text()!= "") //check no boxes are null
    {
        if(start.X < r && start.Y < c && goal.X < r && goal.Y < c)
        {
            Board board(r,c,LoadTextFile(_fileName,r,c));//debug
           // Board board(20,20,LoadTextFile(fileName,20,20)); //release
            int heuristic = 0;
            if(this->ui->comboBoxHeuristic->currentText().toStdString() == "manhatten")
            {
                heuristic = 0;
            }
            else if(this->ui->comboBoxHeuristic->currentText().toStdString() == "euclidean")
            {
                heuristic = 1;
            }
            else if(this->ui->comboBoxHeuristic->currentText().toStdString() =="chebyshev")
            {
                heuristic = 2;
            }
            else
            {
                heuristic = 0;
            }
            if(this->ui->radioButtonFullVisualisation->isChecked())
            {
                 pathVisualise(board,start,goal,0,heuristic,this->ui->graphicsViewGrid);
            }
            else
            {
                path(board,start,goal,0,heuristic);
            }
            }
        else
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Error");
            msgBox.setInformativeText("path is out of range");
            msgBox.exec();
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setInformativeText("Please set the start and goal coordinates and try again");
        msgBox.exec();
    }
    }
    catch(...)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setInformativeText("An error has occured, please check your coordinates and grid size and try again");
        msgBox.exec();
    }
}

void GridWindow::on_pushButtonOpenFile_clicked()
{
    try
    {
    setFileName(QFileDialog::getOpenFileName(this, tr("Open File"),
                                                     "",
                                                     tr("Files (*.*)")).toStdString());
    initiateGrid();
    }
    catch(...)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setInformativeText("Could not load image, maybe the rows and columns are incorrectly set?");
        msgBox.exec();
    }
}
