#include "NodeList.h"

NodeList::NodeList()
{
	//nothing much needs doing as this class is a wrapper on a vector with a few custom methods
}

//unlike erase, this WILL call delete on all contained pointers, this is so stop memory leaks but could mean a pointer is deleted thats in use elsewhere
NodeList::~NodeList()
{
	for( vector<Node*>::iterator i = _list.begin(); i != _list.end(); ++i ) //Makes sure all the nodes contained within are wiped out when the list goes out of scope.		
	{
		delete *i;
	}
}

Node* NodeList::getItem(int x, int y)
{
	for(int i = 0; i < _list.size();i++)
	{
		if((*_list[i]).X() == x && (*_list[i]).Y() == y) {return _list[i];}
	}
}

bool NodeList::inList(Node* node)
{
	for(int i = 0; i < _list.size();i++)
	{
		if((*_list[i]).X() == (*node).X() && (*_list[i]).Y() == (*node).Y()) {return true;}
	}
	return false;
}



//Its important to remember this only removes the node from the list, it does NOT call delete on the contained node pointer, incase its in use elsewhere
void NodeList::erase(int x, int y) 
{
	for(int i =0; i < _list.size(); i++)
	{
		Node& node = *_list[i];
		if(node.X() == x && node.Y() == y)
		{
			//delete list[i];
			_list.erase(_list.begin() + i);
		}
	}
}

void NodeList::push_back(Node* node) 
{
	_list.push_back(node);
}

void NodeList::addNew(int x, int y, int cost)
{
	Node* newNode = new Node(x,y,cost);
	_list.push_back(newNode);
}

void NodeList::remove(int x, int y)
{
	for(int i =0; i < _list.size(); i++)
	{
		Node& node = *_list[i];
		if(node.X() == x && node.Y() == y)
		{
			//delete list[i];
			_list.erase(_list.begin() + i);
		}
	}
}

int NodeList::size()
{
	return _list.size();
}

Node* NodeList::operator[](int i)
{
	return _list[i];
}