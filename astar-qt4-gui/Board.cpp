#include "Board.h"


//Private methods
//path finding
double Board::euclideanDistance(int currentX, int currentY,int goalX, int goalY)
{
	Coordinates current(currentX,currentY);
	Coordinates goal(goalX,goalY);
	return euclideanDistance(current,goal);
}
double Board::euclideanDistance(Coordinates current,Coordinates goal)
{
	return sqrt(pow(current.X-goal.X,2.0) + pow(current.Y-goal.Y,2.0));
}
double Board::manhattenDistance(int currentX, int currentY,int goalX, int goalY)
{
	Coordinates current(currentX,currentY);
	Coordinates goal(goalX,goalY);
	return manhattenDistance(current,goal);
}
double Board::manhattenDistance(Coordinates current,Coordinates goal)
{
	return (abs(current.X-goal.X) + abs(current.Y-goal.Y));
}

Board::Board(int r, int c) : TwoDimensionalArray(r,c)
{
}

Board::Board(int r, int c, int* d) : TwoDimensionalArray(r,c,d)
{
}

//PATH FINDING METHODS
vector<Coordinates> Board::findPath(int startX, int startY, int goalX, int goalY,string type,string heuristic)
{
	Coordinates start(startX, startY);
	Coordinates goal(goalX,goalY);
	return findPath(start,goal,type,heuristic);
}

vector<Coordinates> Board::findPath(Coordinates start, Coordinates goal, string type,string heuristic)
{
	NodeList openList;
	NodeList closedList;
	vector<Coordinates> bestPath;
	if(type == "astar")
	{
		bestPath = astar(start,goal,heuristic);
	}
	return bestPath;
}

void Board::generateSuccessors(Coordinates goal, NodeList& openList, NodeList& closedList, Node* parent,string heuristic)
{
	int x = (*parent).X();
	int y = (*parent).Y();
	for(int i = x - 1; i<=x+1;i++)
	{
		for(int j = y - 1; j<= y +1;j++)
		{
			if(i != x || j != y)
			{
				if(GetItem(i,j) != 0 && i >=0 && j >=0 )
				{
				//	cout<<"instance: "<<i<<" "<<j<<endl;
					//double h = manhattenDistance(i,j,goalX,goalY);
					double h = 0.0;
					if(heuristic == "manhatten")
					{
						h = manhattenDistance(i,j,goal.X,goal.Y);

					}
					if(heuristic == "euclidean")
					{
						h = euclideanDistance(i,j,goal.X,goal.Y);

					}
					else
					{
                        h = manhattenDistance(i,j,goal.X,goal.Y);
					}
					
					Node* newNode =  new Node(i,j,GetItem(i,j),h,parent);
					if(closedList.inList(newNode)) // if its already in the closed list
					{
						//update the existing one to the new parent and g value
						Node* current = closedList.getItem((*newNode).X(),(*newNode).Y());
						if( (*newNode).G() < (*current).G())
						{
							(*current).G((*newNode).G());
							(*current).Parent((*newNode).Parent());
							delete newNode;
						}
					}
					else if(openList.inList(newNode)) // if its already in the open list
					{
						//update the existing one to the new parent and p value
						Node* current = openList.getItem((*newNode).X(),(*newNode).Y());
						if( (*newNode).G() < (*current).G())
						{
							(*current).G((*newNode).G());
							(*current).Parent((*newNode).Parent());
							delete newNode;
						}
					}
					else
					{
						openList.push_back(newNode);
					}		
				}
			}
		}
	}

}

//A STAR SPECIFIC METHODS

vector<Coordinates> Board::astar(Coordinates start, Coordinates goal,string heuristic) //Finds path using astar algorithm, using specified heuristic
{
	vector<Coordinates> bestPath; //the path of nodes that reach the target
	NodeList closedList; //nodes that have already been checked
	NodeList openList;//nodes that need evaluating
	Node* startNode = new Node(start.X,start.Y,GetItem(start.X,start.Y));//generates a start node
	(*startNode).G(euclideanDistance(start.X,start.Y,goal.X, goal.Y)); //probably best to set the heuristic distance from the start node, in case the algorith thinks its a good idea to try going back across it for some reason if it were 0
	openList.push_back(startNode); // puts the start node onto the open list
	generateSuccessors(start,openList,closedList,startNode,heuristic); //adds all adjacent nodes from the start node to the open list
	openList.remove(start.X,start.Y); //removes start node from open list
	closedList.addNew(start.X,start.Y,GetItem(start.X,start.Y)); // places start node in closed list
	
	bool finished = false;
	while(true) // while there are still nodes to be evaluated
	{
		Node* candidate = openList[0];
		for(int i = 0; i < openList.size();i++) 
		{
			Node& currentNode = *openList[i];
            //if( currentNode.F() < (*candidate).F() && (currentNode.X() != (*startNode).X() && currentNode.Y() != (*startNode).Y()) ) // finds the node with the lowest f value
            if( currentNode.F() < (*candidate).F())
			{
				candidate = openList[i];
			}
		}
	//	cout<<(*candidate).X()<< " " <<(*candidate).X()<<endl;
		 //if candidate is the goal then just run with it, and plop that, and all of its parents into the bestPath list. at least thats what I think its doing, could be wrong
		if((*candidate).X() == goal.X && (*candidate).Y() == goal.Y)
		{
			
			Node* node = candidate;
			Coordinates newCoord((*node).X(),(*node).Y());
			bestPath.push_back(newCoord);
			
			while(true)
			{
				if((*node).Parent() != 0)
				{
					//cout<<(*node).Parent()<<endl;
					//system("pause");
					Node newNode = *(*node).Parent(); // have to deferenence the pointer first, ill be damned if i am returning pointers.
					newCoord.X = newNode.X();
					newCoord.Y = newNode.Y();
					bestPath.push_back(newCoord);
					node = (*node).Parent();
				}
				else
				{
					
					break;
				}
			}
			break;
		}
		else
		{
			closedList.push_back(candidate);
			openList.remove( (*candidate).X(), (*candidate).Y());
			generateSuccessors(goal,openList,closedList,candidate,heuristic);
			//system("pause");
		}
		
	}

	//puts open list into bestPath list purely for testing purpouses
	//removeAllFromList(openList);
	//for(int i =0; i < openList.size();i++)
	//{
	//	cout<<i<<endl;
	//	Node newNode = *openList[i];
	//	//system("pause");
	//	bestPath.push_back(newNode);
	//}
	return bestPath;
}
