#ifndef GRIDWINDOW_H
#define GRIDWINDOW_H

//#include <QMainWindow>

namespace Ui {
class GridWindow;
}

class GridWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit GridWindow(QWidget *parent = 0);

    ~GridWindow();
    
private slots:
    void on_buttonPath_clicked();

private:
    Ui::GridWindow *ui;
    void initiateGrid();
};

#endif // GRIDWINDOW_H
