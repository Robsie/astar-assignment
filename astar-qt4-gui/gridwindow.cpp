#include "gridwindow.h"
#include "ui_gridwindow.h"

#include <QGraphicsScene>
#include <QMessageBox>
#include <qstring>
#include <vector>
#include "Files.h"
#include "Board.h"


GridWindow::GridWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GridWindow)
{
    ui->setupUi(this);
    initiateGrid();
}

GridWindow::~GridWindow()
{
    delete ui;
}

void GridWindow::initiateGrid()
{
     Board board(11,8,LoadTextFile("D:\\programming\\repositories\\astar-assignment\\astar-qt4-gui\\test.txt",11,8));
      QGraphicsScene* scene = new QGraphicsScene;
     for(int i =0;i<board.Rows();i++)
     {
         for(int j =0; j<board.Columns();j++)
         {
             if(board.GetItem(i,j) == 0)
             {
                  scene->addRect(QRectF(20*(i), 20*(j), 20, 20), QPen(Qt::black), QBrush(Qt::black));
             }
             else
             {
                  scene->addRect(QRectF(20*(i), 20*(j), 20, 20), QPen(Qt::black), QBrush(Qt::white));
             }
         }
     }
     this->ui->graphicsViewGrid->setScene(scene);

}

void GridWindow::on_buttonPath_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("hello");

    int startX = this->ui->textBoxStartX->text().toInt();
    int startY = this->ui->textBoxStartY->text().toInt();
    int goalX = this->ui->textBoxGoalX->text().toInt();
    int goalY = this->ui->textBoxGoalY->text().toInt();


    Board board(11,8,LoadTextFile("D:\\programming\\repositories\\astar-assignment\\astar-qt4-gui\\test.txt",11,8));

    QGraphicsScene* scene = new QGraphicsScene;
    string heuristic = this->ui->comboBoxHeuristic->currentText().toStdString();


    msgBox.setInformativeText(QString::fromStdString(heuristic));


    if(heuristic == "manhatten")
    {
        msgBox.setInformativeText("manhatten");

    }
    if(heuristic == "euclidean")
    {
        msgBox.setInformativeText("euclidean");

    }
    else
    {
        msgBox.setInformativeText("else");
    }
    msgBox.exec();



    vector<Coordinates> path = board.findPath(startX,startY,goalX,goalY,"astar",heuristic);


    //Fill in the grid with rectangles, white for passable black for impassable
    for(int i =0;i<board.Rows();i++)
    {
        for(int j =0; j<board.Columns();j++)
        {
            if(board.GetItem(i,j) == 0)
            {
                 scene->addRect(QRectF(20*(i), 20*(j), 20, 20), QPen(Qt::black), QBrush(Qt::black));
            }
            else
            {
                 scene->addRect(QRectF(20*(i), 20*(j), 20, 20), QPen(Qt::black), QBrush(Qt::white));
            }
        }
    }

    for(int i = 0; i < path.size();i++) //Colour in the path
    {
        scene->addRect(QRectF(20*(path[i].X), 20*(path[i].Y), 20, 20), QPen(Qt::black), QBrush(Qt::blue));
    }

     scene->addRect(QRectF(20*(startX), 20*(startY), 20, 20), QPen(Qt::black), QBrush(Qt::green)); //Colour start node
     scene->addRect(QRectF(20*(goalX), 20*(goalY), 20, 20), QPen(Qt::black), QBrush(Qt::red)); //Colour end node


    // Fit the view in the scene's bounding rect
    this->ui->graphicsViewGrid->setScene(scene);

}
