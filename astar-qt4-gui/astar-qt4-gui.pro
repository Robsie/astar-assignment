#-------------------------------------------------
#
# Project created by QtCreator 2013-04-27T23:10:31
#
#-------------------------------------------------

QT       += core gui

TARGET = astar-qt4-gui
TEMPLATE = app


SOURCES += main.cpp\
        gridwindow.cpp \
    NodeList.cpp \
    Node.cpp \
    Files.cpp \
    Board.cpp

HEADERS  += gridwindow.h \
    TwoDimensionalArray.h \
    NodeList.h \
    Node.h \
    Files.h \
    Board.h

FORMS    += gridwindow.ui
