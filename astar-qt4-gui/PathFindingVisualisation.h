#ifndef PATHFINDINGVISUALISATION_H
#define PATHFINDINGVISUALISATION_H
#include <QGraphicsView>
#include "Board.h"


vector<Coordinates> AStarVisualise(Coordinates start, Coordinates goal, int heuristic,QGraphicsView& view);

void generateSuccessors(Coordinates goal, NodeList& openList, NodeList& closedList, Node* parent,string heuristic,QGraphicsView& view);

vector<Coordinates> VisualisePath(int startX, int startY, int goalX, int goalY,int type, int heuristic,QGraphicsView& view); // find path using specified algorithm and heuristic type passing ints
vector<Coordinates> VisualisePath(Coordinates start, Coordinates goal, int type,int heuristic,QGraphicsView& view); // find path using specified algorithm and heuristic type passing Coordinates

#endif // PATHFINDINGVISUALISATION_H
