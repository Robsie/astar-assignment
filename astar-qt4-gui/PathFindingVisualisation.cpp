#include "PathFindingVisualisation.h"

vector<Coordinates> AStarVisualise(Coordinates start, Coordinates goal,string heuristic,QGraphicsView& view)
{
    vector<Coordinates> bestPath;
    return bestPath;
}

void generateSuccessors(Coordinates goal, NodeList& openList, NodeList& closedList, Node* parent,int heuristic,QGraphicsView& view)
{

}

vector<Coordinates> VisualisePath(int startX, int startY, int goalX, int goalY,int type, int heuristic,QGraphicsView& view)// find path using specified algorithm and heuristic type passing ints
{
    Coordinates start(startX, startY);
    Coordinates goal(goalX,goalY);
    return VisualisePath(start,goal,type,heuristic,view);
}

vector<Coordinates> VisualisePath(Coordinates start, Coordinates goal, int type,int heuristic,QGraphicsView& view)// find path using specified algorithm and heuristic type passing Coordinates
{
    NodeList openList;
    NodeList closedList;
    vector<Coordinates> bestPath;
    if(type == 0)
    {
        bestPath = AStarVisualise(start,goal,heuristic,view);
    }
    return bestPath;
}

