#pragma once
#ifndef NODE_H
#define NODE_H


using namespace std;

class Node
{
private:
	int _x;
	int _y;
	/*int _pX;
	int _pY;*/
	Node* _parent;
	double _f;
	double _h; //_h is the distance from the goal node
	double _g; //_g is the distance travelled already
	int _cost;

	
public:
	//constructors
	Node(int x, int y, int cost);
	Node(int x, int y, int cost, double g, Node* parent);
	Node(); 
	//getters and setters
	int X() const;
	void X(int);
	int Y() const;
	void Y(int);

	Node* Parent() const;
	void Parent(Node*);
	/*int PX() const;
	void PX(int);
	int PY() const;
	void PY(int);*/

	double F() const;
	void F(double);
	double H() const;
	void H(double);
	double G() const;
	void G(double);

	int Cost() const;
	void Cost(int);

	Node& operator=(const Node& n);
};

#endif