#pragma once
#ifndef TWODIMENSIONALARRAY_H
#define TWODIMENSIONALARRAY_H
using namespace std;

template<class T>
class TwoDimensionalArray
{
protected:
	T* _data;
	int _columns;
	int _rows;
public:
	TwoDimensionalArray(int r, int c, T* d);
	TwoDimensionalArray(int r, int c);
	int Rows() const;
	int Columns() const;
	void SetItem(int r, int c, T d);
	T GetItem(int r, int c);
};
#endif


template<class T> TwoDimensionalArray<T>::TwoDimensionalArray(int r, int c, T* d)
{
	_rows = r;
	_columns = c;
	_data = new T[r*c];
	for(int i = 0; i < r*c;i++)
	{
		_data[i] = d[i];
	}
}

template<class T> TwoDimensionalArray<T>::TwoDimensionalArray(int r, int c)
{
	_rows = r;
	_columns = c;
	_data = new T[r*c];
	for(int i = 0; i < r*c;i++)
	{
		_data[i] = 0;
	}
}

template<class T> int TwoDimensionalArray<T>::Rows() const
{
	return _rows;
}

template<class T> int TwoDimensionalArray<T>::Columns() const
{
	return _columns;
}

template<class T> void TwoDimensionalArray<T>::SetItem(int r, int c,T d)
{
	 _data[x + (y * _rows)] = d;
}

template<class T> T TwoDimensionalArray<T>::GetItem(int r, int c)
{
	return _data[r + (c * _rows)];
}