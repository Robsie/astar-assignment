#pragma
#ifndef Board_H
#define Board_H
#include <cmath>
#include <string>
#include <vector>
#include "TwoDimensionalArray.h"
#include "TwoDimensionalArray.h"
#include "Node.h"
#include "NodeList.h"

using namespace std;

struct Coordinates
{
	int X;
	int Y;
	Coordinates(int x, int y)
	{
		X = x;
		Y = y;
	}
};

class Board : public TwoDimensionalArray<int>
{
private:
	//HEURISTIC METHODS
	double euclideanDistance(int currentX, int currentY,int goalX, int goalY);
	double euclideanDistance(Coordinates current,Coordinates goal);
	double manhattenDistance(int currentX, int currentY,int goalX, int goalY);
	double manhattenDistance(Coordinates current,Coordinates goal);
	// Private PATH FINDING METHODS
	void generateSuccessors(Coordinates goal, NodeList& openList, NodeList& closedList, Node* parent,string heuristic);
	// Private A STAR SPECIFIC METHODS
	vector<Coordinates> astar(Coordinates start, Coordinates goal,string heuristic); //Finds path using astar algorithm, using specified heuristic	
public:
	Board(int r, int c);
	Board(int r, int c, int* d);
	//PUBLIC PATH FINDING METHODS
	vector<Coordinates> findPath(int startX, int startY, int goalX, int goalY,string type, string heuristic); // find path using specified algorithm and heuristic type passing ints
	vector<Coordinates> findPath(Coordinates start, Coordinates goal, string type,string heuristic); // find path using specified algorithm and heuristic type passing Coordinates
};

#endif