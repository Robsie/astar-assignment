#include "Files.h"

void OutputPGMImage(string fname, double* d, int r, int c, int q)
{
	
 int i;
 unsigned char *image;
 ofstream myfile;
 image = (unsigned char *) new unsigned char [r*c];

 // convert the integer values to unsigned char 
 for(i=0; i<r*c; i++)
 {
	//image[i]=(unsigned char)filterNoise(data[i]);	 
	image[i]=(unsigned char)d[i];	 

 }

 myfile.open(fname, ios::out|ios::binary|ios::trunc);

 if (!myfile) {
   cout << "Can't open file: " << fname << endl;
   exit(1);
 }

 myfile << "P5" << endl;
 myfile << r << " " << c << endl;
 myfile << q << endl;

 myfile.write( reinterpret_cast<char *>(image), (r*c)*sizeof(unsigned char));

 if (myfile.fail()) {
   cout << "Can't write image " << fname << endl;
   exit(0);
 }
 myfile.close();
 delete [] image;
}

int* LoadTextFile(string fname, int r, int c)
{
	int i=0;
	int* data = new int[r*c];
	ifstream myfile (fname);
	if (myfile.is_open())
	{
		while ( myfile.good())
	{
	if (i>r*c-1) break;
		myfile >> *(data+i);
		i++;                                                             
	}
	myfile.close();
	}
	else cout << "Unable to open file"; 
	return data;
}
